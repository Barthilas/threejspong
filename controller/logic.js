// get options from menu feel free to change
var url_string = window.location.href;
var url = new URL(url_string);
var difficulty = url.searchParams.get("AI"); // set opponent reflexes (0 - easiest, 1 - hardest)
var ballSpeed = url.searchParams.get("BallSpeed"); //ball speed
var maxScore = url.searchParams.get("score"); //score
var multiplayer = JSON.parse(url.searchParams.get("multiplayer"));

// scene object variables
var renderer, scene, camera, pointLight, spotLight;

// field variables
var fieldWidth = 400, fieldHeight = 200;

// paddle variables
var paddleWidth, paddleHeight, paddleDepth, paddleQuality;
var paddle1DirY = 0, paddle2DirY = 0, paddleSpeed = 3;

// ball variables
var ball, paddle1, paddle2;
var ballDirX = 1, ballDirY = 1;

// game-related variables
var playerScore = 0, CPUscore = 0; finished = false

//audios
audios = ["../materials/gun.mp3", "../materials/slap.mp3"]

function setup() {
	// Display statistics of drawing to canvas
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	stats.domElement.style.zIndex = 100;
	document.body.appendChild(stats.domElement);

	// update the board to reflect the max score for match win
	document.getElementById("winnerBoard").innerHTML = "First to " + maxScore + " wins!";

	// now reset player and opponent scores
	playerScore = 0;
	CPUscore = 0;

	// set up all the 3D objects in the scene	
	createScene();

	// and let's get cracking!
	draw();
}

function createScene() {
	// set the scene size
	var WIDTH = 640,
		HEIGHT = 360;

	// set some camera attributes
	var VIEW_ANGLE = 50,
		ASPECT = WIDTH / HEIGHT,
		NEAR = 0.1,
		FAR = 10000;

	var c = document.getElementById("gameCanvas");

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer();
	camera =
		new THREE.PerspectiveCamera(
			VIEW_ANGLE,
			ASPECT,
			NEAR,
			FAR);

	scene = new THREE.Scene();

	// add the camera to the scene
	scene.add(camera);

	// set a default position for the camera
	// not doing this somehow messes up shadow rendering
	camera.position.z = 320;

	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	c.appendChild(renderer.domElement);

	// set up the playing surface 
	var clothWidth = fieldWidth,
		clothHeight = fieldHeight,
		clothQuality = 10;

	// create the player paddle material
	var PlayerPaddleMaterial =
		new THREE.MeshLambertMaterial(
			{
				color: 0x16259e
			});
	// create the AI paddle material
	var AIPaddleMaterial =
		new THREE.MeshLambertMaterial(
			{
				color: 0x9e1515
			});
	// create the cloth's material
	var texture = new THREE.TextureLoader().load('../materials/pool_table_@2X.png');
	var clothMaterial =
		new THREE.MeshLambertMaterial(
			{
				map: texture
			});
	// create the table's material
	var tableMaterial =
		new THREE.MeshLambertMaterial(
			{
				color: 0x111111
			});
	// create the pillar's material
	var texture = new THREE.TextureLoader().load('../materials/arroway.de_tiles-27_d100_2.jpg');
	var pillarMaterial =
		new THREE.MeshLambertMaterial(
			{
				map: texture
			});
	// create the ground's material
	var texture = new THREE.TextureLoader().load('../materials/Marble.png');
	texture.crossOrigin = true;
	var groundMaterial =
		new THREE.MeshLambertMaterial(
			{
				map: texture,
				overdraw: 0.5
			});


	// create the playing surface
	var plane = new THREE.Mesh(

		new THREE.PlaneGeometry(
			clothWidth * 0.95,
			clothHeight,
			clothQuality,
			clothQuality),

		clothMaterial);

	scene.add(plane);
	plane.receiveShadow = true;

	//create table
	var table = new THREE.Mesh(

		new THREE.CubeGeometry(
			clothWidth * 1.05,
			clothHeight * 1.03,
			100, //Z
			clothQuality,
			clothQuality,
			1),

		tableMaterial);
	table.position.z = -51;	// we sink the table into the ground by 50 units. The extra 1 is so the plane can be seen
	scene.add(table);
	table.receiveShadow = true;

	// // set up the sphere vars
	// lower 'segment' and 'ring' values will increase performance
	var radius = 5,
		segments = 6,
		rings = 6;

	// // create the sphere's material
	var sphereMaterial =
		new THREE.MeshLambertMaterial(
			{
				color: 0xffffff
			});

	// Create a ball with sphere geometry
	ball = new THREE.Mesh(

		new THREE.SphereGeometry(
			radius,
			segments,
			rings),

		sphereMaterial);

	// // add the sphere to the scene
	scene.add(ball);
	ball.position.x = 0;
	ball.position.y = 0;
	// set ball above the table surface
	ball.position.z = radius;
	ball.receiveShadow = true;
	ball.castShadow = true;

	// // set up the paddle vars
	paddleWidth = 10;
	paddleHeight = 30;
	paddleDepth = 10;
	paddleQuality = 1;

	paddle1 = new THREE.Mesh(

		new THREE.CubeGeometry(
			paddleWidth,
			paddleHeight,
			paddleDepth,
			paddleQuality,
			paddleQuality,
			paddleQuality),

		PlayerPaddleMaterial);

	// add player paddle to the scene
	scene.add(paddle1);
	paddle1.receiveShadow = true;
	paddle1.castShadow = true;

	paddle2 = new THREE.Mesh(

		new THREE.CubeGeometry(
			paddleWidth,
			paddleHeight,
			paddleDepth,
			paddleQuality,
			paddleQuality,
			paddleQuality),

		AIPaddleMaterial);

	// add player paddle to the scene
	scene.add(paddle2);
	paddle2.receiveShadow = true;
	paddle2.castShadow = true;

	// set paddles on each side of the table
	paddle1.position.x = -fieldWidth / 2 + paddleWidth;
	paddle2.position.x = fieldWidth / 2 - paddleWidth;

	// lift paddles over playing surface
	paddle1.position.z = paddleDepth;
	paddle2.position.z = paddleDepth;

	//Left pillars
	for (var i = 0; i < 5; i++) {
		var pillar = new THREE.Mesh(

			new THREE.CylinderGeometry(
				30, //width
				30, //height
				300, //z
			),
			pillarMaterial);

		pillar.rotation.x = Math.PI / 2;
		pillar.position.x = -50 + i * 100;
		pillar.position.y = 230;
		pillar.position.z = -30;
		pillar.castShadow = true;
		pillar.receiveShadow = true;
		scene.add(pillar);
	}
	//Right pillars
	for (var i = 0; i < 5; i++) {
		var pillar = new THREE.Mesh(

			new THREE.CylinderGeometry(
				30,
				30,
				300,
			),
			pillarMaterial);

		pillar.rotation.x = Math.PI / 2;
		pillar.position.x = -50 + i * 100;
		pillar.position.y = -230;
		pillar.position.z = -30;
		pillar.castShadow = true;
		pillar.receiveShadow = true;
		scene.add(pillar);
	}

	// finally we finish by adding a ground plane
	// to show off pretty shadows

	var ground = new THREE.Mesh(

		new THREE.CubeGeometry(
			1000,
			1000,
			3,
			1,
			1,
			1),
		groundMaterial);
	// set ground to arbitrary z position to best show off shadowing
	ground.position.z = -132;
	ground.receiveShadow = true;
	scene.add(ground);

	// // create a point light
	pointLight =
		new THREE.PointLight(0xF8D898);

	// set its position
	pointLight.position.x = -1000;
	pointLight.position.y = 0;
	pointLight.position.z = 1000;
	pointLight.intensity = 2;
	pointLight.distance = 10000;
	// add to the scene
	scene.add(pointLight);

	// add a spot light
	// this is important for casting shadows
	spotLight = new THREE.SpotLight(0xF8D898);
	spotLight.position.set(0, 0, 460);
	spotLight.intensity = 1.5;
	spotLight.castShadow = true;
	scene.add(spotLight);

	// MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC <- better shadows.
	renderer.shadowMapEnabled = true;

	var listener = new THREE.AudioListener();
	scene.add(listener);

	// create a global audio source
	var sound = new THREE.Audio(listener);

	// load a sound and set it as the Audio object's buffer
	var audioLoader = new THREE.AudioLoader();
	audioLoader.load('/materials/ambient.mp3', function (buffer) {
		sound.setBuffer(buffer);
		sound.setLoop(true);
		sound.setVolume(0.3);
		sound.play();
	});

	//var axesHelper = new THREE.AxesHelper( 5 );
	//scene.add( axesHelper );
}
function draw() {
	// draw THREE.JS scene
	renderer.render(scene, camera);
	// loop draw function call
	requestAnimationFrame(draw);

	ballPhysics();
	paddlePhysics();
	cameraPhysics();
	playerPaddleMovement();
	opponentPaddleMovement();
	stats.update();
}

function ballPhysics() {
	// if ball goes off the 'left' side (Player's side)
	if (ball.position.x <= -fieldWidth / 2) {
		CPUscore++;
		document.getElementById("scores").innerHTML = playerScore + "-" + CPUscore;
		resetBall(2);
		matchScoreCheck();
	}

	// if ball goes off the 'right' side (CPU's side)
	if (ball.position.x >= fieldWidth / 2) {
		playerScore++;
		document.getElementById("scores").innerHTML = playerScore + "-" + CPUscore;
		resetBall(1);
		matchScoreCheck();
	}

	// if ball goes off the top side (side of table)
	if (ball.position.y <= -fieldHeight / 2) {
		ballDirY = -ballDirY;
	}
	// if ball goes off the bottom side (side of table)
	if (ball.position.y >= fieldHeight / 2) {
		ballDirY = -ballDirY;
	}
	// update ball position over time
	ball.position.x += ballDirX * ballSpeed;
	ball.position.y += ballDirY * ballSpeed;
}

// Handles CPU paddle movement and logic
function opponentPaddleMovement() {
	if (multiplayer == false) {
		// Lerp towards the ball on the y plane
		paddle2DirY = (ball.position.y - paddle2.position.y) * difficulty;

		// in case the Lerp function produces a value above max paddle speed, we clamp it
		if (Math.abs(paddle2DirY) <= paddleSpeed) {
			paddle2.position.y += paddle2DirY;
		}
		// if the lerp value is too high, we have to limit speed to paddleSpeed
		else {
			// if paddle is lerping in +ve direction
			if (paddle2DirY > paddleSpeed) {
				paddle2.position.y += paddleSpeed;
			}
			// if paddle is lerping in -ve direction
			else if (paddle2DirY < -paddleSpeed) {
				paddle2.position.y -= paddleSpeed;
			}
		}
	}
	//opponent as human.
	else {
		// move left
		if (Key.isDown(Key.Arrow_left)) {
			// if paddle is not touching the side of table -> move
			// /2 is too small hence 0,45
			if (paddle2.position.y < fieldHeight * 0.45) {
				paddle2DirY = paddleSpeed * 0.5;
			}
			// else we don't move and stretch the paddle
			// to indicate we can't move
			else {
				paddle2DirY = 0;
			}
		}
		// move right
		else if (Key.isDown(Key.Arrow_right)) {
			// if paddle is not touching the side of table
			if (paddle2.position.y > -fieldHeight * 0.45) {
				paddle2DirY = -paddleSpeed * 0.5;
			}
			// else we don't move and stretch the paddle
			// to indicate we can't move
			else {
				paddle2DirY = 0;
			}
		}
		// else don't move paddle
		else {
			// stop the paddle
			paddle2DirY = 0;
		}
		//update position of player paddle
		paddle2.position.y += paddle2DirY;
	}
}

tmp = ballSpeed
// Handles player's paddle movement
function playerPaddleMovement() {
	//Pause
	if (Key.isDown(Key.W)) {
		ballSpeed = 0
	}
	else {
		if (finished == false) {
			ballSpeed = tmp
		}
	}
	// move left
	if (Key.isDown(Key.A)) {
		// if paddle is not touching the side of table -> move
		// /2 is too small hence 0,45
		if (paddle1.position.y < fieldHeight * 0.45) {
			paddle1DirY = paddleSpeed * 0.5;
		}
		// else we don't move and stretch the paddle
		// to indicate we can't move
		else {
			paddle1DirY = 0;
		}
	}
	// move right
	else if (Key.isDown(Key.D)) {
		// if paddle is not touching the side of table
		if (paddle1.position.y > -fieldHeight * 0.45) {
			paddle1DirY = -paddleSpeed * 0.5;
		}
		// else we don't move and stretch the paddle
		// to indicate we can't move
		else {
			paddle1DirY = 0;
		}
	}
	// else don't move paddle
	else {
		// stop the paddle
		paddle1DirY = 0;
	}
	//update position of player paddle
	paddle1.position.y += paddle1DirY;
}

// Handles camera and lighting logic
function cameraPhysics() {
	// we can easily notice shadows if we dynamically move lights during the game
	spotLight.position.x = ball.position.x * 2;
	spotLight.position.y = ball.position.y * 2;

	if(multiplayer==false)
	{
	// move to behind the player's paddle
	camera.position.x = paddle1.position.x - 100;
	camera.position.y += (paddle1.position.y - camera.position.y) * 0.05;
	camera.position.z = paddle1.position.z + 100 + 0.04 * (paddle1.position.x);

	// rotate to face towards the opponent
	camera.rotation.x = -0.01 * (paddle1.position.y) * Math.PI / 180;
	camera.rotation.y = -60 * Math.PI / 180;
	camera.rotation.z = -90 * Math.PI / 180;
	}
	else
	{
		//mutiplayer camera above table
		camera.position.z = 300
	}
}

// Handles paddle collision logic
function paddlePhysics() {
	bc = new THREE.Box3().setFromObject(ball);
	//var helper = new THREE.Box3Helper( bc, 0xffff00 );
	//scene.add( helper );
	p1c = new THREE.Box3().setFromObject(paddle1);
	//var helper = new THREE.Box3Helper( p1c, 0xffff00 );
	//scene.add( helper );
	p2c = new THREE.Box3().setFromObject(paddle2);

	// Collision logic
	//prevent player from abusing collision boxes (and condition)
	if (p1c.intersectsBox(bc) && ball.position.x >= -fieldWidth / 2 + paddleWidth || p2c.intersectsBox(bc)) {
		var audio = new Audio(audios[Math.floor(Math.random() * audios.length)]);
		audio.volume = 0.2;
		audio.play()
		// switch direction of ball travel to create bounce
		ballDirX = -ballDirX;
		// we impact ball angle when hitting it
		// this is not realistic physics, just spices up the gameplay
		// allows you to 'slice' the ball to beat the opponent
		ballDirY -= paddle1DirY * 0.7;
	}
}

function resetBall(loser) {
	// position the ball in the center of the table
	ball.position.x = 0;
	ball.position.y = 0;

	// if player lost the last point, we send the ball to opponent
	if (loser == 1) {
		ballDirX = -1;
	}
	// else if opponent lost, we send ball to player
	else {
		ballDirX = 1;
	}

	// set the ball to move +ve in y plane (towards left from the camera)
	ballDirY = 1;
}
// checks if either player or opponent has reached required points
function matchScoreCheck() {

	if (playerScore >= maxScore) {
		finished = true
		// stop the ball
		ballSpeed = 0;
		// write to the banner
		document.getElementById("scores").innerHTML = "Blue wins!";
		document.getElementById("winnerBoard").innerHTML = "Refresh to play again";
	}
	else if (CPUscore >= maxScore) {
		finished = true
		// stop the ball
		ballSpeed = 0;
		// write to the banner
		document.getElementById("scores").innerHTML = "Red wins!";
		document.getElementById("winnerBoard").innerHTML = "Refresh to play again";
	}
}